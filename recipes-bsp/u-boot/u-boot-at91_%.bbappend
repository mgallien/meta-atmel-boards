COMPATIBLE_MACHINE      := "(netsam9x25|sama5d3_xplained_light)"

FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"

SRC_URI += " \
             file://0001-activate-net-console-for-at91sam9x5.patch \
             file://0002-activate-the-watchdog-for-the-supported-boards.patch \
             file://0003-remove-build-of-SPL-since-building-SPL-and-the-watch.patch \
           "

